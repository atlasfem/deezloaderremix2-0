# Deezloader Remix
### Latest Version: 4.2.1
Deezloader Remix is an improved version of Deezloader based on the Reborn branch.<br/>
With this app you can download songs, playlists and albums directly from Deezer's Server in a single and well packaged app.

![](https://i.imgur.com/NeOg9YU.png)
## Features
### Base Features
* Download MP3s and FLACs directly from Deezer Servers
* Search and Discover music from the App
* Download music directly from a URL
* Download entire Artists library
* See your public playlist on Deezer
* Tagged music files (ID3s and Vorbis Comments)
* Great UI and UX

### Exclusive to Remix
* Implementation with Spotify APIs (No third party services)
* Improved download speed
* Extensive set of options for a personalized ｅｘｐｅｒｉｅｎｃｅ
* Server mode to launch the app headless
* MOAR Optimizations

## Download
All compiled downloads are on Telegram.<br>
News: [@RemixDevs](https://t.me/RemixDevs)<br>
Downloads: [@DeezloaderRemix](https://t.me/DeezloaderRemix)<br>
Mirros: [Wiki/Downloads](https://notabug.org/RemixDevs/DeezloaderRemix/wiki/Downloads)<br>
Chat: [@DeezloaderRemixCommunity](https://t.me/DeezloaderRemixCommunity)<br>
Here are listed the MD5 checksums (so you can be sure the files were not tampered):<br>

| Checksum MD5                       | Filename                               |
| ---------------------------------- | -------------------------------------- |
| `0b607336f5a332504b937a55903dd988` | Deezloader Remix 4.2.1.exe             |
| `98a3e7e2c050fa695e500f50a7dd22a1` | Deezloader_Remix_4.2.1-i386.AppImage   |
| `6010cb406f8054a3ca6a3bb6acd7a039` | Deezloader Remix 4.2.1 Setup.exe       |
| `70610702c1461b0f7ed76673ff15d77b` | Deezloader Remix 4.2.1 Setup x32.exe   |
| `85a1e843efaf85aa7ef4f8526cf5e049` | Deezloader Remix 4.2.1 x32.exe         |
| `706967580087748e9689b056e495cc0d` | Deezloader_Remix_4.2.1-x86_64.AppImage |
| `4efdd879e3160c221f59bf4135c9f689` | Deezloader Remix-4.2.1.dmg             |

## Build
If you want to buid it yourself you will need Node.js installed, git and npm or yarn.<br/>
To start utilizing the app you should open a terminal inside the project folder and run `npm install`.<br/>
If you want to start the app, without compiling it you can use `npm start`<br/>
To run it in server mode you can use `npm start -- -s` or go inside the `app` folder and use `node app.js`<br/>
To build the app for other OSs follow the table below

| OS                 | Command              |
| ------------------ | -------------------- |
| Windows x64        | `npm run dist:win64` |
| Windows x32 or x86 | `npm run dist:win32` |
| Linux              | `npm run dist:linux` |
| macOS              | `npm run dist:macOS` |

## Disclaimer
I am not responsible for the usage of this program by other people.<br/>
I do not recommend you doing this illegally or against Deezer's terms of service.<br/>
This project is licensed under [GNU GPL v3](https://www.gnu.org/licenses/gpl-3.0.html)
